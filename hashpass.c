#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "md5.h"

int main(int argc, char *argv[])
{
    //Open Files with Error Handling
    if(argc < 3)
    {
        printf("Please specify a source file and destination file.");
        return 2;
    }
    
    FILE *sourceFile = fopen(argv[1], "r");
    if(!sourceFile)
    {
        printf("Unable to open %s for reading.", argv[1]);
        return 3;
    }
    
    FILE *destinationFile = fopen(argv[2], "w");
    if(!destinationFile)
    {
        printf("Unable to open %s for writing", argv[2]);
        return 4;
    }
    
    //Array for Plain Text Password
    char plainText[16];
    
    //Main Program Loop
    while(fgets(plainText, 16, sourceFile))
    {
        //Remove '\n', Hash Password, Print to File, and Free Memory
        plainText[strlen(plainText) -1] = '\0';
        char *hashedText = md5(plainText, strlen(plainText));
        fprintf(destinationFile, "%s\n", hashedText);
        free(hashedText);
    }
    
    //Close Files
    fclose(sourceFile);
    fclose(destinationFile);
}